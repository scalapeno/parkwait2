/*
 * Copyright (c) 2019 Randy Unger
 */

// import * as Themeparks from "themeparks"
import { Park, Parks } from "themeparks"

// const DisneyWorldMagicKingdom = new Themeparks.Parks.WaltDisneyWorldMagicKingdom()
// const SixFlagsHH = new Themeparks.Parks.SixFlagsHurricaneHarborLosAngeles()

// Access wait times by Promise
const CheckWaitTimes = (park: Park) => {
    console.log("Checking times MK")
    park.GetWaitTimes().then((rideTimes) => {
        console.log(`Got times MK for ${rideTimes.length} rides`)
        rideTimes.forEach((ride) => {
            console.log(`${ride.name}: ${ride.waitTime} minutes wait (${ride.status})`)
        })
    }).catch((error) => {
        console.error(error)
    }).then(() => {
        // setTimeout(CheckWaitTimes, 1000 * 60 * 5) // refresh every 5 minutes
        setTimeout(CheckWaitTimes(park), 1000 * 15) // refresh every 5 minutes
    })
}

// const parkList =  Themeparks.AllParks

const testList = ["SixFlagsWhiteWaterAtlanta", "UniversalStudiosSingapore"]

const CheckAllWaitTimes = () => {
    // parkList.forEach((park) => {
    //     console.log(park.toString())
    // })
    console.log("parks: " + JSON.stringify(Parks))

    Object.keys(Parks).forEach((park) => {
        // console.log(park.name)
        if (testList.includes(park)) {

            const pp = new Parks[park]()
            console.log("Park name: " + park)
            pp.GetOpeningTimes().then(opening => {
                console.log("opens: " + opening[0].openingTime)
                console.log("closes: " + opening[0].closingTime)
            }) //CheckOpeningTime(pp)
        }

        // CheckWaitTimes(pp)
    })
}

export { CheckAllWaitTimes, CheckWaitTimes } //, parkList }
// Access wait times by Promise
// const CheckWaitTimesHH = () => {
//     console.log("Checking times HH")
//     SixFlagsHH.GetWaitTimes().then((rideTimes) => {
//         console.log(`Got times HH for ${rideTimes.size} rides`)
//         rideTimes.forEach((ride) => {
//             console.log(`${ride.name}: ${ride.waitTime} minutes wait (${ride.status})`)
//         })
//     }).catch((error) => {
//         console.error(error)
//     }).then(() => {
//         // setTimeout(CheckWaitTimes, 1000 * 60 * 5) // refresh every 5 minutes
//         setTimeout(CheckWaitTimesHH, 1000 * 15) // refresh every 5 minutes
//     })
// }
//
// export { CheckWaitTimes, CheckWaitTimesHH}