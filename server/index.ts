/*
 * Copyright (c) 2019 Randy Unger
 */

import * as express from 'express'
import * as path from 'path'
import * as morgan from 'morgan'
import * as http from 'http'
import { CheckAllWaitTimes } from "./parkService"

const app = express()
app.set('port', 8000)

app.use(express.static(path.join(__dirname, '..', 'build', 'static')))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(morgan('dev'))

app.get("/test", (req, res) => {
    res.send("Hello World!")
})

app.get("/times", (req, res) => {
    const times = {"a": 5}
    res.json(times)
})

app.all('/*', (req, res, next) => {
    console.log('Reading the main route through http request, sending index.html');
    res.sendFile(path.join(__dirname, '..', 'build', 'index.html'))
})

const server = http.createServer(app)
server.listen(app.get('port'), () => {
    console.log('express listening on port ' + app.get('port'))
})

// const p0 = parkList[0]
// console.log(p0)
//

// CheckWaitTimes(p0)
CheckAllWaitTimes()
// CheckWaitTimesHH()