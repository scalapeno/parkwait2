/*
 * Copyright (c) 2019 Randy Unger
 */

// declare module 'themeparks' {
//     const Parks: ParkObj
//
//     interface WaltDisneyWorldMagicKingdom {
//     }
//
//     interface ParkObj {
//         function WaltDisneyWorldMagicKingdom(): WaltDisneyWorldMagicKingdom
//     }
// }

declare module "themeparks" {
    interface RideWait {
        name: string,
        waitTime: number,
        status: string
    }

    interface Park {
        GetWaitTimes(): Promise<Array<RideWait>>

        GetOpeningTimes(): Promise<Array<OpeningInfo>>
    }

    interface OpeningInfo {
        date: string,
        openingTime: string,
        closingTime: string,
        type: string,
        special: boolean
    }

    const Parks: Array<Park>

}

// export as namespace themeparks;
//
// // export const Parks: ParksObj;
// //
// // export interface ParksObj {
// //     WaltDisneyWorldMagicKingdom: WaltDisneyWorldMagicKingdom
// //     GetWaitTimes: Promise<Array<Ride>>
// // }
//
// export interface Parks {
//     // GetWaitTimes:
//     interface
//     WaltDisneyWorldMagicKingdom
// }
//
// export interface Ride {
//
// }
